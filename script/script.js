'use strict';

function initMap() {
    //マップを生成
    let map = new google.maps.Map(document.getElementById("map"), {
        center: {lat: 35.660207814456236, lng: 139.68457774779907},
        zoom: 16,
    });

    let marker;

    function success(pos) {
        const lat = pos.coords.latitude;
        const lng = pos.coords.longitude;
        map.setCenter({lat: lat, lng: lng});

        //マーカーを設置
        marker = new google.maps.Marker({
            position: map.getCenter(),
            map: map,
            title: "現在地",
        });
    }

    function fail(error) {
        alert('位置情報の取得に失敗しました。エラーコード:' + error.code);
    }

    navigator.geolocation.getCurrentPosition(success, fail);
    
    //'way'のselectが変更されたときに<header>の中身を一部変更
    document.getElementById('form').way.onchange = function() {
        const change = document.getElementById('form').way.value;
        if(change === 'normal') {
            document.querySelector('h1').textContent = "通常検索マップ";
            document.getElementById('start').textContent = "出発地点";
            document.getElementById('end').textContent = "目的地点";
            document.getElementById('way_rad').textContent = "交通手段";
            const way = `<select name="mode">
                            <option value="WALKING" selected>徒歩</option>
                            <option value="DRIVING">車</option>
                        </select> `;
            document.getElementById('opt').innerHTML = way;
            document.getElementById('searchResult').textContent = `検索結果`;
            document.querySelector('option[value="normal"]').selected = true;
            const result = `<iframe id="inlineFrame" src="result.html" width="100%" height="400px"></iframe> `
            document.getElementById('iframe').innerHTML = result;
        } else if(change === 'around') {
            document.querySelector('h1').textContent = "周辺検索マップ";
            document.getElementById('start').textContent = "中心地点";
            document.getElementById('end').textContent = "周辺検索";
            document.getElementById('way_rad').textContent = "検索半径";
            const radius = `<input type="text" name="radius" class="text" placeholder="100~1000(m)">`;
            document.getElementById('opt').innerHTML = radius;
            document.querySelector('option[value="around"]').selected = true;
            const table = `<iframe id="inlineFrame" src="table.html" width="100%" height="400px"></iframe> `
            document.getElementById('iframe').innerHTML = table;
        } else {
            console.log('error');
        }
    };
    
    //情報ウィンドウの生成
    let infoWindow = new google.maps.InfoWindow({
        pixelOffset: new google.maps.Size(0, 5)
    });
    
    //ジオコーディングのインスタンスの生成
    let geocoder = new google.maps.Geocoder();  

    //マップクリック時に発生するイベント
    map.addListener('click', function(event) {
        marker.setMap(null); //以前のマーカーを削除

        this.setCenter(event.latLng);
        this.panTo(event.latLng); //アニメーションで中心位置を移動

        geocoder.geocode({location: event.latLng}, function(results, status){
            if(status === 'OK' && results[0]) {
                //マーカーの生成
                marker = new google.maps.Marker({
                    position: event.latLng,
                    map: map,
                    title: results[0].formatted_address,
                    //animation: google.maps.Animation.DROP
                });
        
                //マーカーにリスナーを設定
                marker.addListener('click', function() {
                    infoWindow.setContent(results[0].formatted_address);
                    infoWindow.open(map, marker);
                });

                marker.addListener('dblclick', function(){
                    this.setMap(null);
                });
                
                //情報ウィンドウリスナーを設定
                /*
                infoWindow.addListener('closeclick', function() {
                    marker.setMap(null);  //マーカーを削除
                });
                */
            } else if(status === 'ZERO_RESULTS') {
                alert('不明なアドレスです： ' + status);
                return;
            } else {
                alert('失敗しました： ' + status);
                return;
            }
        });
    });

    //DirectionsService のオブジェクトを生成
    let directionsService = new google.maps.DirectionsService();
    
    //DirectionsRenderer のオブジェクトを生成
    let directionsRenderer = new google.maps.DirectionsRenderer();

    //directionsRenderer と地図を紐付け
    directionsRenderer.setMap(map); 

    //PlacesService のインスタンスの生成（引数に map を指定）
    let service = new google.maps.places.PlacesService(map);

    //DistanceMatrixServise のインスタンスを生成
    const matrixService = new google.maps.DistanceMatrixService(); 

    let center_marker = new google.maps.Marker();
    let markers = [];

    //検索ボタンを押したときに発生するイベント
    document.getElementById('search').onclick = function() {
        if(document.getElementById('form').way.value === "normal") {
            marker.setMap(null); //マーカーを削除
            center_marker.setMap(null); //センターマーカーを削除
            directionsRenderer.set('directions', null);//directionsRendererをリセット
            for (let i = 0; i < markers.length; i++) {
                markers[i]["marker"].setMap(null);//グリーンマーカーを一つずつ解除
            }

            //リクエストの出発点の位置
            let originN = document.getElementById('form').center.value;
            //リクエストの終着点の位置
            let destN = document.getElementById('form').destination.value; 
            //リクエストのトラベルモード
            let modeN = document.getElementById('form').mode.value;
            
            // ルートを取得するリクエスト
            let request = { 
                origin: originN, // 出発地点の緯度経度
                destination: destN, // 到着地点の緯度経度
                travelMode: modeN, //トラベルモード
            };

            //DirectionsService のオブジェクトのメソッド route() にリクエストを渡し、
            //コールバック関数で結果を setDirections(result) で directionsRenderer にセットして表示
            directionsService.route(request, function(result, status) {
                //ステータスがOKの場合、
                if (status === 'OK') {
                    directionsRenderer.setDirections(result); //取得したルート（結果：result）をセット
                    const distance = result.routes[0].legs[0].distance.text;
                    const duration = result.routes[0].legs[0].duration.text;
                    const end_address = result.routes[0].legs[0].end_address;
                    const start_address = result.routes[0].legs[0].start_address;
                    const directionTable =  `<table>
                                                <tr class="address">
                                                    <th>出発地点住所<br>(A)</th>
                                                    <td>${start_address}</td>
                                                </tr>
                                                <tr class="address">
                                                    <th>目的地点住所<br>(B)</th>
                                                    <td>${end_address}</td>
                                                </tr>
                                                <tr class="info">
                                                    <th>道のり</th>
                                                    <td>${distance}</td>
                                                </tr>
                                                <tr class="info">
                                                    <th>所要時間</th>
                                                    <td>${duration}</td>
                                                </tr>
                                            </table>`;
                    document.getElementById('inlineFrame').contentWindow.document.getElementById("table3").innerHTML = directionTable;
                } else {
                    alert("取得できませんでした：" + status);
                }
            });
        } else if(document.getElementById('form').way.value === "around") {
            const radius = document.getElementById('form').radius.value;
            const radiusNum = Number(radius);//radiusをint型に変換(無理だった場合はNaNになる)
            if(isNaN(radiusNum)) {
                window.alert("「検索半径」には半角数字(100~1000)を入力して下さい。");
            } else if(radiusNum < 200 || 1000 < radiusNum) {
                window.alert("「検索半径」には100~1000の値を入力して下さい。");
            } else {
                marker.setMap(null); //マーカーを削除
                center_marker.setMap(null); //センターマーカーを削除
                directionsRenderer.set('directions', null);//directionsRendererをリセット
                for (let i = 0; i < markers.length; i++) {
                    markers[i]["marker"].setMap(null);//グリーンマーカーを一つずつ解除
                }

                let address = document.getElementById('form').center.value; 

                //リクエストの終着点の位置
                let name = document.getElementById('form').destination.value;

                //geocoder.geocode() にアドレスを渡して、コールバック関数を記述して処理
                geocoder.geocode({ address: address }, function(results, status){
                    //ステータスが OK で results[0] が存在すれば、locationに格納
                    if (status === 'OK' && results[0]){  
                        let location = results[0].geometry.location;//リクエストの出発点の位置
                        
                        map.setCenter(location);

                        //種類（タイプ）やキーワードをもとに施設を検索（プレイス検索）するメソッド nearbySearch()
                        service.nearbySearch({
                            location: location,  //検索するロケーション
                            radius: radiusNum,  //検索する半径（メートル）
                            name: name,  //タイプで検索。文字列またはその配列で指定
                            //キーワードで検索する場合は name:'レストラン' や ['レストラン','中華'] のように指定
                        }, callback);  //コールバック関数（callback）は別途定義
                        
                        //コールバック関数には results, status が渡されるので、status により条件分岐
                        function callback(results, status) {
                            // status は以下のような定数で判定（OK の場合は results が配列で返ってきます）
                            if (status === google.maps.places.PlacesServiceStatus.OK) {
                                //検索結果の数を表示
                                document.getElementById('searchResult').textContent = `検索結果: ${results.length} 件`;

                                center_marker = new google.maps.Marker({
                                    map: map,
                                    position: location, //results[i].geometry.location
                                    label: {
                                        text: "C" ,
                                        color: "black" ,
                                        fontSize: "20px" ,
                                        fontWeight: "bold"
                                    }
                                });

                                const origins = [location];
                                let destinations = [];//目的地の座標を入れておく配列

                                //results の数だけ for 文で繰り返し処理
                                for (let i = 0; i < results.length; i++) {
                                    //createMarker() はマーカーを生成する関数（別途定義）
                                    markers[i] = {"marker": createMarker(results[i])};
                                    destinations.push(results[i].geometry.location);
                                }

                                const matrixOptions = {
                                    origins: origins, //中心地点
                                    destinations: destinations, //PlacesServiseから受け取ったLatLng
                                    travelMode: 'WALKING',
                                };

                                //Distance Matrix serviceの呼び出し
                                matrixService.getDistanceMatrix(matrixOptions, callback);

                                function callback(response, status) {
                                    if (status !== "OK") {
                                        alert("Error with distance matrix");
                                        return;
                                    }

                                    let trInsert = document.getElementById('inlineFrame').contentWindow.document.getElementById('table2');
                                    trInsert.innerHTML=`<tr>
                                                            <th class="fixed" class="destName"><h4>目的地名</h4></th>
                                                            <th class="fixed"><h4>道のり</h4></th>
                                                            <th class="fixed"><h5>所要時間</h5></th>
                                                        </tr>`;

                                    let routes = response.rows[0].elements;
                                    for(let i = 0; i < results.length; i++) {
                                        const distance = routes[i].distance.value + " m";
                                        const duration = routes[i].duration.text;
                                        const tr = `<tr>
                                                        <td class="destName"><a href="#" id="${i}">${results[i].name}</a></td>
                                                        <td>${distance}</td>
                                                        <td>${duration}</td>
                                                    </tr>`;
                                        
                                        trInsert.insertAdjacentHTML('beforeend', tr);

                                        
                                    }
                                    
                                    let destNames = document.getElementById('inlineFrame').contentWindow.document.querySelectorAll('#table2 a');
                                    destNames.forEach(function(item, index) {
                                        item.onclick = function(event) {
                                            event.preventDefault();
                                            destNames.forEach(function(item, index) {
                                                item.style.color = "#d25833";
                                            });
                                            this.style.color = "purple";

                                            directionsRenderer.set('directions', null);//directionsRendererをリセット
                                            for(let i = 0; i < results.length; i++){
                                                //全てのマーカーを表示
                                                markers[i]["marker"].setVisible(true);
                                            }

                                            const originA = location;
                                            const destA = destinations[index];

                                            // ルートを取得するリクエスト
                                            let request = { 
                                                origin: originA, // 出発地点の緯度経度
                                                destination: destA, // 到着地点の緯度経度
                                                travelMode: "WALKING", //トラベルモード
                                            };

                                            //DirectionsService のオブジェクトのメソッド route() にリクエストを渡し、
                                            //コールバック関数で結果をsetDirections(result) で directionsRenderer にセットして表示
                                            directionsService.route(request, function(result, status) {
                                                //ステータスがOKの場合、
                                                if (status === 'OK') {
                                                    center_marker.setVisible(false);//中心のマーカーを非表示
                                                    markers[index]["marker"].setVisible(false);//選択した地点のマーカーを非表示
                                                    // start_address のテキストを変更
                                                    result.routes[0].legs[0].start_address = document.getElementById('form').center.value; 
                                                    // end_address のテキストを変更
                                                    result.routes[0].legs[0].end_address = results[index].name;

                                                    directionsRenderer.setDirections(result); //取得したルート（結果：result）をセット
                                                } else {
                                                    alert("取得できませんでした：" + status);
                                                }
                                            });
                                        }
                                    });
                                }
                            }
                        }
                        
                        //マーカーを生成する関数（引数には検索結果の配列 results[i] が入ってきます）
                        function createMarker(place) {
                            let green_marker = new google.maps.Marker({
                                map: map,
                                position: place.geometry.location, //results[i].geometry.location
                                icon: {
                                    url: "https://maps.google.com/mapfiles/ms/micons/green-dot.png",
                                    scaledSize: new google.maps.Size(30, 30)
                                },
                            });
                        
                            //マーカーにイベントリスナを設定
                            green_marker.addListener('click', function() {
                                infoWindow.setContent(place.name);  //results[i].name
                                infoWindow.open(map, this);
                            });

                            return green_marker;
                        }
                        
                    }else{ 
                        //ステータスが OK 以外の場合や results[0] が存在しなければ、アラートを表示して処理を中断
                        alert('失敗しました。理由: ' + status);
                        return;
                    }
                });
            }   
        } else {
            console.log("Search Type Error");
        }
    };
}